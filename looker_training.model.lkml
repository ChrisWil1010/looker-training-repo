connection: "looker_training"

# include all the views
include: "*.view"

# include all the dashboards
include: "*.dashboard"

datagroup: looker_training_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: looker_training_default_datagroup

explore: customer {
  join: nation {
    relationship: one_to_one
    type: inner
    sql_on: ${customer.c_nationkey}=${nation.n_nationkey} ;;
  }
}
explore: orders {
  join: customer {
    relationship: one_to_one
    type: inner
    sql_on: ${customer.c_custkey}=${orders.o_custkey} ;;
  }
}
